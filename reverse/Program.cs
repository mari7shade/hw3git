﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reverse
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding= Encoding.UTF8;
            int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Console.WriteLine("Массив");
            Console.WriteLine(string.Join(", ",array));
            Console.WriteLine("Інвертований массив");
            Array.Reverse(array);
            Console.WriteLine(string.Join(", ", array));

            //delay
            Console.ReadKey();
        }
    }
}
