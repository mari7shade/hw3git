﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inverted
{
    internal class Program
    {
        static int[] MyReverse(int[] array)
        {
            Array.Reverse(array);
            return array;
        }
        static int[] SubArray(int[] array, int index, int count)
        {
            int[] result = new int[count];
            int a = 0;
            for (int i = index; a<count; i++)
            {
                
                if (a>=(array.Length - index))
                {
                    result[a] = 1;
                }
                else result[a] = array[i];
                a++;

            }
            return result;
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Массив");
            int[] mounth = {25, 52, 123, 5888, 2155, 33, 544, 22, 252, 321, 8, 1, 10, 75};
            for (int i = 0;i < mounth.Length; i++)
            {
                Console.Write(mounth[i]);
                if (i != (mounth.Length -1))
                {
                    Console.Write(", ");
                }
            }

            Console.WriteLine();
            Console.WriteLine("Інвертований массив:");

            int[] reverse = MyReverse(mounth);

            
            Console.Write(string.Join(", ", reverse));
            //for (int i = 0; i < reverse.Length; i++)
            //{
            //    Console.Write(mounth[i]);
            //    if (i != (mounth.Length - 1))
            //    {
            //        Console.Write(", ");
            //    }
            //}


            Console.WriteLine();
            int index, count;
            Console.WriteLine("Введіть значення індексу массива");
            string indexBeforeConvert = Console.ReadLine();
            Int32.TryParse(indexBeforeConvert, out index);
            Console.WriteLine("Введіть розмір массиву");
            string countBeforeConvert = Console.ReadLine();
            Int32.TryParse(countBeforeConvert, out count);

            int[] newArray = SubArray(mounth, index, count);
            for (int i = 0; i< newArray.Length; i++)
            {
                Console.Write(newArray[i]);
                if (i != (newArray.Length - 1))
                {
                    Console.Write(", ");
                }
            }
            


            //delay
            Console.ReadKey();
        }
    }
}
