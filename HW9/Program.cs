﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW9
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding= Encoding.UTF8;
            Console.WriteLine("Массив");
            int[] array = { 25,354,44,58,21,2,545,85,69,7445,312,5,11,28 };
            int min = array[0], max = array[0], summ=0;
            for (int i = 0;i < array.Length; i++)
            {
                Console.Write($"{array[i]}, ");
                if (array[i] < min)
                {
                    min = array[i];
                }
                if (array[i] > max)
                {
                    max = array[i];
                }
                summ = array[i]+summ;
            }
            Console.WriteLine();
            Console.WriteLine("Найменьше значення массиву: " + min);
            Console.WriteLine("Найбільше значення массиву: " + max);
            Console.WriteLine("Загальна сумма всіх єлементів: " + summ);
            Console.WriteLine("Середнє арифметичне всіх елементів: " + summ/array.Length);
            Console.WriteLine("Усі непарні значення:");
            for (int i = 0;i < array.Length; i++)
            {
                if (array[i]%2 !=0) {
                    Console.Write($"{array[i]}, ");
                }
            }



            //Delay 
            Console.ReadKey();
        }
    }
}
