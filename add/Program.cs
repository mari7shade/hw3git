﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace add
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding= Encoding.UTF8;
            int[] array = { 1, 2, 3, 4, };
            while (true)
            {
                Console.WriteLine(string.Join(", ", array));
                Console.WriteLine("Введіть нову змінну масиву");
                string valueBeforeConvert = Console.ReadLine();
                int value;
                Int32.TryParse(valueBeforeConvert, out value);
                array = Add(array, value);
                Console.WriteLine(string.Join(", ", array));


                Console.ReadLine();
            }
            
        }
        static int[] Add(int[] array, int value)
        {
            int[] result = new int[array.Length+1];
            result[0] = value;
            for (int i =1 ; i < result.Length; i++) {
                result[i] = array[i-1];
            }
            return result;
        }
    }
}
